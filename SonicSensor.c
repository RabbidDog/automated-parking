/*
 * SonicSensor.c
 *
 *  Created on: Nov 27, 2015
 *      Author: Anjum Parvez Ali
 */

#include "head.h"
#include "SonicSensor.h"
#include "VehicleProperty.h"
#include <system.h>
#define BufferActualSize 100
#define smoothBlockSize 5;

int blockSize = smoothBlockSize;
float actualReading[BufferActualSize];
//float smoothReading[BufferActualSize];
int readHead,smoothHead;

unsigned int MeasureDistanceSingleDevice(unsigned int ch)
{
	float x = (float)pUltraSoundArray[ch] * 330000/ALT_CPU_FREQ;
	return( (unsigned int)(x/2) );
}
/*http://www.embedded.com/design/configurable-systems/4007651/DSP-Tricks-Smoothing-impulsive-noise*/
void SonicMeasurementSmoothing(double* smoothBuffer, float arithmeticMean)
{
	int Pos =0;
	int Neg = 0;
	double DTotal = 0.0;
	int i = 0;
	//printf("current read head %i", readHead);
	//printf("current mean %f", arithmeticMean);
	for(i = readHead-blockSize; i< readHead; i++)
	{
		if(actualReading[i] > arithmeticMean)
		{
			Pos++;
			DTotal += actualReading[i] - arithmeticMean;
		}
		else
		{
			Neg++;
		}
	}
	smoothBuffer[smoothHead++] = (arithmeticMean + ((Pos-Neg)*DTotal)/(blockSize * blockSize))/10;//in cm
}

/*writes the smooth data in the buffer provided by calling function
 * Calling function should send the index from where to start writing
 * method returns the number of values written*/
int ReadSonicSensorDataIntoBuffer(double* smoothBuffer, int index, int sensorId)
{
	/*currently read from sensor
	 * later move to reading memory
	 * That will allow parallel read and smoothing execution
	 * Assumption: outliers are ignored*/
	/*Assumption is the module reads data dumped in memory every second and there are 40 readings there each time*/
	int i = 0;
	readHead = 0;
	smoothHead = index;

	while(readHead < 40)
	{
		double currentMean = 0.0;

		for(i = 0; i<blockSize;i++)
		{
			while (*pHc_sr04 != 0xff);
			*pHc_sr04 = 0xff;
			while (*pHc_sr04 != 0xff);
			actualReading[readHead] = MeasureDistanceSingleDevice(sensorId); /*assuming that parking is always on the right*/
			currentMean += actualReading[readHead];
			readHead++;
		}
		SonicMeasurementSmoothing(smoothBuffer, currentMean / blockSize);
	}
	//after 40 readings have been smoothed to 8 readings

	/*print the actual values*/
	/*for ( i = 0; i < 400; i++)
	{
		printf("%u \t %f\n",i, actualReading[i]);
	}
	printf("\n smooth values \n");
	for( i = 0;i<40;i++)
	{
		printf("%u \t %f\n",i, smoothReading[i]);
	}*/
	return 40/blockSize;
}

void readAllRangeSensor(int* buffer)
{
	while (*pHc_sr04 != 0xff);
	*pHc_sr04 = 0xff;
	while (*pHc_sr04 != 0xff);
	int index;
	for(index = 0; index < vehicle_sensor_count; index++)
	{
		buffer[index] = MeasureDistanceSingleDevice(index);
	}
}
