/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: parkNeuralNetworkFunction_terminate.c
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 11-Jan-2016 01:22:18
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "parkNeuralNetworkFunction.h"
#include "parkNeuralNetworkFunction_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void parkNeuralNetworkFunction_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for parkNeuralNetworkFunction_terminate.c
 *
 * [EOF]
 */
