/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: parkNeuralNetworkFunction_types.h
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 11-Jan-2016 01:22:18
 */

#ifndef __PARKNEURALNETWORKFUNCTION_TYPES_H__
#define __PARKNEURALNETWORKFUNCTION_TYPES_H__

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for parkNeuralNetworkFunction_types.h
 *
 * [EOF]
 */
