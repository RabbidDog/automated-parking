/*
 * ParkingModule.h
 *
 *  Created on: Mar 5, 2016
 *      Author: RabbidDog
 */


#ifndef PARKINGMODULE_H_
#define PARKINGMODULE_H_

#include <stdio.h>
#include "head.h"
#include "math.h"
#include "GenerateMap.h"
#include "DriveControl.h"

struct movementCommand
{
	double final_angle_change; //+ive for anti-clockwise -ive for clockwise
	double straight_distance; //this is not the actual distance covered. This is the distance between the start and end points
};
void ParkingModuleInit();
void startToPark(struct spotProperties properties);

#endif /* PARKINGMODULE_H_ */
