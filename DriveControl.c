/*
 * DriveControl.c
 *
 *  Created on: Feb 25, 2016
 *      Author: Anjum Parvez Ali
 *      This class manages the movement of the vehicle.
 *
 */

#include "DriveControl.h"

volatile unsigned int fr, rr, rl, fl;
int attempts = 0;
void DriveControlInit()
{
	fr = rr = rl = fl = 0;
}

void avoidCollision()
{
	refreshMap();





}
void tillAngleTurned(double angle)
{
	//approximate delay as gyro is not working
	delay((angle / 90) * 13500000);
	//make call to function that reads gyroscope value over a period of time
}
void carStop()
{
	*pwm_enable = PAUSE_ENC_MASK;
	*pwm_enable = 0;
}
void carMoveForward()
{
	set_duty_cycle(pFrontRightDutySet, 22);
	set_duty_cycle(pRearRightDutySet, 22);
	set_duty_cycle(pRearLeftDutySet, 22);
	set_duty_cycle(pFrontLeftDutySet, 22);

	*pwm_enable = (ALL_WHEEL_FWD_MASK | ENABLE_ENC_MASK );
}
void carMoveBackwards(double distance)
{
	set_duty_cycle(pFrontRightDutySet, 22);
	set_duty_cycle(pRearRightDutySet, 22);
	set_duty_cycle(pRearLeftDutySet, 22);
	set_duty_cycle(pFrontLeftDutySet, 22);
	if(distance > 1)
	{
		int ticksToCover = distanceToWheelTicks(distance);
		fr = ticksToCover;
		rr = ticksToCover;
		rl = ticksToCover;
		fl = ticksToCover;
		*pwm_enable = (ALL_WHEEL_BWD_MASK| PLAY_BACK_MASK | ENABLE_ENC_MASK );
	}
	else
	{
		*pwm_enable = (ALL_WHEEL_BWD_MASK | ENABLE_ENC_MASK );
	}

}
void carMoveForwardTo(double distance, double angle)
{
	set_duty_cycle(pFrontRightDutySet, 35);
	set_duty_cycle(pRearRightDutySet, 35);
	set_duty_cycle(pRearLeftDutySet, 35);
	set_duty_cycle(pFrontLeftDutySet, 35);
	if(angle > 0 && angle < 91)
	{
		*pwm_enable = (ONLY_LEFT_WHEELS_BWD_MASK | ONLY_RIGHT_WHEELS_FWD_MASK | ENABLE_ENC_MASK);
		tillAngleTurned(angle);
		*pwm_enable = 0;
	}
	if(angle > 270) //turn left
	{
		*pwm_enable = (ONLY_LEFT_WHEELS_FWD_MASK | ONLY_RIGHT_WHEELS_BWD_MASK | ENABLE_ENC_MASK );
		tillAngleTurned(angle);
		*pwm_enable = 0;
	}

	if(distance > 0)
	{
		int ticksToCover = distanceToWheelTicks(distance);

		fr = ticksToCover;
		rr = ticksToCover;
		rl = ticksToCover;
		fl = ticksToCover;

		*pwm_enable = (ALL_WHEEL_FWD_MASK | PLAY_BACK_MASK | ENABLE_ENC_MASK );
	}
}

/*Parameter angle represents the absolute destination angle wit respect to the vehicle
 * It does not represent the actual angle the car has to turn to reach the orientation*/
void carMoveBackwardTo(double distance, double angle)
{
	set_duty_cycle(pFrontRightDutySet, 35);
	set_duty_cycle(pRearRightDutySet, 35);
	set_duty_cycle(pRearLeftDutySet, 35);
	set_duty_cycle(pFrontLeftDutySet, 35);
	if(angle > 270 && angle < 360)
	{

		printf("move backwards to angle %f\n", angle);
		/*set_duty_cycle(pFrontRightDutySet, 35);
		set_duty_cycle(pRearRightDutySet, 35);
		set_duty_cycle(pRearLeftDutySet, 30);
		set_duty_cycle(pFrontLeftDutySet, 30);*/
		*pwm_enable = (ONLY_RIGHT_WHEELS_BWD_MASK | ONLY_LEFT_WHEELS_FWD_MASK | PAUSE_ENC_MASK );
		tillAngleTurned(360-angle); // need to verify the gyro functionality. Clockwise might be -ive rading
		*pwm_enable = 0;
		printf("Done move backwards to angle %f\n", angle);

	}
	else if(angle > 0 && angle < 90)
	{
		/*set_duty_cycle(pFrontRightDutySet, 30);
		set_duty_cycle(pRearRightDutySet, 30);
		set_duty_cycle(pRearLeftDutySet, 30);
		set_duty_cycle(pFrontLeftDutySet, 30);*/

		*pwm_enable = (TURN_LEFT_MASK | PAUSE_ENC_MASK );

		tillAngleTurned(angle - 180); // need to verify the gyro functionality
		*pwm_enable = 0;
	}

	if(distance > 0)
	{
		/*set_duty_cycle(pFrontRightDutySet, 30);
		set_duty_cycle(pRearRightDutySet, 30);
		set_duty_cycle(pRearLeftDutySet, 30);
		set_duty_cycle(pFrontLeftDutySet, 30);*/
		int ticks = distanceToWheelTicks(distance);
		printf("Moving back distance %f with wheel ticks %i\n", distance, ticks);
		*pFrontRightEncSet = ticks;
		*pRearRightEncSet = ticks;
		*pRearLeftEncSet = ticks;
		*pFrontLeftEncSet = ticks;
		*pwm_enable = (ALL_WHEEL_BWD_MASK | PLAY_BACK_MASK | ENABLE_ENC_MASK);
		while (!(*pwm_enable & WHEEL_READY_MASK));
		*pwm_enable = 0;
		printf("Done moving back distance %f with wheel ticks %i\n", distance, ticks);
	}
}

/*Not verified*/
int getWheelTick()
{
	fr = *pFrontRightEncRead;
	rr = *pRearRightEncRead;
	rl = *pRearLeftEncRead;
	fl = *pFrontLeftEncRead;

	/*find the smallest
	 * because the others might have slipped and give higher count*/
	int retValue = fr;
	if(retValue > rr)
		retValue = rr;
	if(retValue > rl)
		retValue = rl;
	if(retValue > fl)
		retValue = fl;
	return retValue;
}

