/*
 * HMC5883L.c
 *	Encapsulates all operations with the Magnetometer
 *  Created on: Nov 8, 2015
 *      Author: Anjum Parvez Ali
 */

#include "HMC5883L.h"
#include "head.h"
#include "MPU60X0.h" //since magnetometer is conected to auxilary I2C of MPU, we need to enable bypass mode in MPU
#include<math.h>
#include <stdio.h>

#define PI 3.14159265
float m_scale;

void SetMeasurementMode(enum Magnetometer_Mode mode)
{
	I2CWrite(HMC5883L_SLAVE_ADDRESS, HMC5883L_Mode_Register, mode);
}
void SetScale(enum Magnetometer_Scale scale)
{
	//get the true value of the magnetic field by multiplying with the Digital resolution
	//corresponding to the set gain
	char registerValue;
	switch(scale)
	{
	case scale088:
		m_scale = 0.73;
		registerValue = 0x0;
		break;
	case scale13:
		m_scale = 0.92;
		registerValue = 0x1;
		break;
	case scale19:
		m_scale = 1.22;
		registerValue = 0x2;
		break;
	case scale25:
		m_scale = 1.52;
		registerValue = 0x3;
		break;
	case scale40:
		m_scale = 2.27;
		registerValue = 0x4;
		break;
	case scale47:
		m_scale = 2.56;
		registerValue = 0x5;
		break;
	case scale56:
		m_scale = 3.03;
		registerValue = 0x6;
		break;
	case scale81:
		m_scale = 4.35;
		registerValue = 0x7;
		break;
	};

	registerValue = registerValue << 5; //bit 7-5 are to be set and the rest must be cleared
	I2CWrite(HMC5883L_SLAVE_ADDRESS, HMC5883L_Configuration_Register_B, registerValue);
}
void EnableBypass()
{
	I2CWriteBit(MPU_SLAVE_ADDRESS, MPU6050_RA_INT_PIN_CFG, MPU6050_INT_PIN_CFG_I2C_BYPASS_EN, 1); //set bypass enable bit
	/*verify*/

	//printf("%i",I2CReadBit(MPU_SLAVE_ADDRESS, MPU6050_RA_INT_PIN_CFG, MPU6050_INT_PIN_CFG_I2C_BYPASS_EN) );
	I2CWriteBit(MPU_SLAVE_ADDRESS, MPU6050_RA_USER_CTRL, MPU6050_USERCTRL_I2C_MST_EN_BIT, 0); //clear master mode bit
	//printf("%i", I2CReadBit(MPU_SLAVE_ADDRESS, MPU6050_RA_USER_CTRL, MPU6050_USER_CTRL_I2C_MST_EN));
}
void initMagnetometer()
{
	//printf("initMagnetometer: \n");
	EnableBypass();
	SetScale(scale13);
	SetMeasurementMode(continuous);
}


struct Magnetometer ReadRawAxis()
{
	char cBuff[32];
	struct Magnetometer reading;
	I2CRead(HMC5883L_SLAVE_ADDRESS, HMC5883L_X_MSB_Register, 6, cBuff);
	// convert 16-bit 2�s compliment hex values to decimal values
	reading.X_Axis = (cBuff[0] << 8) | (cBuff[1] & 0xff);
	reading.Z_Axis = (cBuff[2] << 8) | (cBuff[3] & 0xff);
	reading.Y_Axis = (cBuff[4] << 8) | (cBuff[5] & 0xff);

	return reading;
}

struct Magnetometer ReadScaledAxis()
{
	struct Magnetometer reading = ReadRawAxis();
	struct Magnetometer scaledReading;
	scaledReading.X_Axis = reading.X_Axis * m_scale;
	scaledReading.Z_Axis = reading.Z_Axis * m_scale;
	scaledReading.Y_Axis = reading.Y_Axis * m_scale;

	return scaledReading;
}
float GetMagnetometerHeading()
{
	struct Magnetometer scaledReading = ReadScaledAxis();
	float heading = atan2(scaledReading.Y_Axis, scaledReading.X_Axis);
	//How to make sure that the device is leveled OR if that is even required?
	//compensate for positive magnetic declination to the east
	//headingDegrees += Magnetic_Declination; //determine if need and if declination added or subtracted
	if(heading < 0)
		heading += 2 * PI;
	// Check for wrap due to addition of declination.
	//if(heading > 2*PI)
	//heading -= 2 * PI;
	float headingDegrees = heading * 180 / PI;

	return headingDegrees;
}
unsigned int IsMagnetometerDataReady()
{
	char bit;
	I2CReadBit(HMC5883L_SLAVE_ADDRESS, HMC5883L_Status_Register, HMC5883L_Status_Register_RDY, &bit);
	return bit;
}
