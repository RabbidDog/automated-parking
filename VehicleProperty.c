/*
 * VehicleDimension.c
 *
 *  Created on: Feb 19, 2016
 *      Author: RabbidDog
 */


#include "VehicleProperty.h"
#include<Math.h>

short vehicleProperty_init_called=0;
int vehicle_length = 26;
int vehicle_width = 16;
int vehicle_sensor_count = 8;
int vehicle_buffer_around = 3;
double wheel_tick_to_cm = 0.43975f;
struct position sensor_positions[8];
void vehiclePropertyInit()
{
	vehicleProperty_init_called = 1;
	//for the current vehicle we start with the front right sensor
	//and go clockwise
	//the current values are just speculations. Modify to reflect the actual value
	// all values are in mm and rad


	sensor_positions[0].x_pos = 13;
	sensor_positions[0].y_pos = 0;
	sensor_positions[0].z_angle = 0;

	sensor_positions[1].x_pos = 13;
	sensor_positions[1].y_pos = -8;
	sensor_positions[1].z_angle = (345*M_PI)/180;

	sensor_positions[2].x_pos = 0;
	sensor_positions[2].y_pos = -8;
	sensor_positions[2].z_angle = (270*M_PI)/180;

	sensor_positions[3].x_pos = -13;
	sensor_positions[3].y_pos = -8;
	sensor_positions[3].z_angle = (195*M_PI)/180;

	sensor_positions[4].x_pos = -13;
	sensor_positions[4].y_pos = 0;
	sensor_positions[4].z_angle = (180*M_PI)/180;

	sensor_positions[5].x_pos = -13;
	sensor_positions[5].y_pos = 8;
	sensor_positions[5].z_angle = (165*M_PI)/180;

	sensor_positions[6].x_pos = 0;
	sensor_positions[6].y_pos = 8;
	sensor_positions[6].z_angle = (90*M_PI)/180;

	sensor_positions[7].x_pos = 13;
	sensor_positions[7].y_pos = 8;
	sensor_positions[7].z_angle = (15*M_PI)/180;

	/*initialize the transformation matrix*/
	int i;
	for(i = 0; i < vehicle_sensor_count; i++)
	{
		sensor_positions[i].T_matrix[0][0] = cos(sensor_positions[i].z_angle);
		sensor_positions[i].T_matrix[0][1] = -sin(sensor_positions[i].z_angle);

		sensor_positions[i].T_matrix[1][0] = sin(sensor_positions[i].z_angle);
		sensor_positions[i].T_matrix[1][1] = cos(sensor_positions[i].z_angle);
	}
}

double* transformedSensorReading(int sensorId, double* x_y_coordinates)
{
	if(!vehicleProperty_init_called)
	{
		printf("vehiclePropertyInit() needs to be called before any of its functions are used\n");
		exit(1);
	}
	double transformedValue[2];
	/*We are not making homogenous transformations (where X and Y position is added as well)
	 * We are finding out the angular direction of the obstacle relative to the vehicle body*/
	transformedValue[0] =  sensor_positions[sensorId].T_matrix[0][0]*x_y_coordinates[0] + sensor_positions[sensorId].T_matrix[0][1]*x_y_coordinates[1];
	transformedValue[1] =  sensor_positions[sensorId].T_matrix[1][0]*x_y_coordinates[0] + sensor_positions[sensorId].T_matrix[1][1]*x_y_coordinates[1];

	return transformedValue;
}

/*distance is generally from the vehicle body and not vehicle co-ordinate 0,0*/
double* getRadialTransformedPosition(int sensorId, double distance)
{
	if(!vehicleProperty_init_called)
	{
		printf("vehiclePropertyInit() needs to be called before any of its functions are used\n");
		exit(1);
	}
	double position[] = {distance, 0};
	double *returnValue = transformedSensorReading(sensorId, position);
	double arc = atan2(returnValue[1], returnValue[0]);
	double dist =  sqrt((returnValue[1]*returnValue[1]) + (returnValue[0]*returnValue[0]));

	returnValue[0] = dist; //in cm
	double angle = (arc * 180)/M_PI;
	if(angle < 0)
		angle += 360;
	returnValue[1] = angle;

	return returnValue;
}

double wheelTicksToDistance(int wheelTick)
{
	return (wheelTick * wheel_tick_to_cm);
}
int distanceToWheelTicks(double distance)
{
	return (int)(distance / wheel_tick_to_cm);
}
