/*
 * GenerateMap.h
 *
 *  Created on: 29.01.2016
 *      Author: Anjum Parvez Ali / Joshua Koutny
 */

// sample values for now

#ifndef GENERATEMAP_H_
#define GENERATEMAP_H_
#include "head.h"

extern char *mapDistanceWeightedAngularCell;
extern struct spotProperties spot_property;
void GenerateMapInit();
void setParkingSpotCoordinate(struct spotProperties *sp);
enum parkingSpotType calcDimensions(double * scanBuffer, int startIndex, int endIndex);
void refreshMap();
#endif



