/*
 * DriveControl.h
 *
 *  Created on: Feb 25, 2016
 *      Author: Anjum Parvez Ali
 *
 */

#ifndef DRIVECONTROL_H_
#define DRIVECONTROL_H_
#include <stdio.h>
#include "head.h"
#include "VehicleProperty.h"
#include "GenerateMap.h"
struct drive_command
{
	double angle;
	double distance;
};
void DriveControlInit();
/*Stops the vehicle at the current spot*/
void carStop();
/*continues to move straight ahead*/
void carMoveForward();
/*continues to move backwards*/
void carMoveBackwards(double distance);
/*moves a certain distance at the angle provided*/
void carMoveForwardTo(double distance, double angle);
/*moves a certain distance at the angle provided
 * But imitates a backing up motion
 * If angle is not in the range of 90-270 then no motion occures*/
void carMoveBackwardTo(double distance, double angle);
/*reset wheel tick*/
//void resetWheelTick();
/*read current wheel tick
 * does not reset the tick count*/
int getWheelTick();


#endif /* DRIVECONTROL_H_ */
