/*
 * VehicleDimension.h
 *
 *  Created on: Feb 19, 2016
 *      Author: Anjum Parvez Ali
 */

#ifndef VEHICLEPROPERTY_H_
#define VEHICLEPROPERTY_H_

/*length is defined in cm. This value will be used to generate a map of the immediate
*environment around the vehicle
*/
extern int vehicle_length;
extern int vehicle_width;
extern int vehicle_sensor_count;
extern int vehicle_buffer_around; // buffer zone to the vehicle body
struct position
{
	/*to get the coordinate respective to base coordinate,use the following formula
	 * x = x_pos + T_matrix[0][0]*x_value + T_matrix[0][1]*y_value
	 * y = y_pos + T_matrix[1][0]*x_value + T_matrix[1][1]*y_value*/
	double x_pos;
	double y_pos;
	double z_angle;
	double T_matrix[2][2];
};
extern struct position sensor_positions[];

void vehiclePropertyInit();
double* transformedSensorReading(int sensorId, double* x_y_coordinates);
/*returns sensor reading relative to car coordinate system
 * position is in distance(cm)and angle(degrees)
 * Distance is relative to the vehicle body and not to the center of the car coordinate system*/
double* getRadialTransformedPosition(int sensorId, double distance);
double wheelTicksToDistance(int wheelTick);
int distanceToWheelTicks(double distance);
#endif /* VEHICLEPROPERTY_H_ */


