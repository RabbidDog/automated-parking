/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: parkNeuralNetworkFunction_terminate.h
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 11-Jan-2016 01:22:18
 */

#ifndef __PARKNEURALNETWORKFUNCTION_TERMINATE_H__
#define __PARKNEURALNETWORKFUNCTION_TERMINATE_H__

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "parkNeuralNetworkFunction_types.h"

/* Function Declarations */
extern void parkNeuralNetworkFunction_terminate(void);

#endif

/*
 * File trailer for parkNeuralNetworkFunction_terminate.h
 *
 * [EOF]
 */
