/*
 * DetectSpot.c
 *
 *  Created on: Jan 9, 2016
 *      Author: Anjum Parvez Ali
 */
#include <stdio.h>
#include <math.h>
#include "DetectSpot.h"
#include "SonicSensor.h"
#include "DriveControl.h"
#include "GenerateMap.h"
#include "VehicleProperty.h"
#include "neuralNetwork/rt_nonfinite.h"
#include "neuralNetwork/parkNeuralNetworkFunction.h"
#include "neuralNetwork/parkNeuralNetworkFunction_terminate.h"
#include "neuralNetwork/parkNeuralNetworkFunction_initialize.h"

#define bufferSize 1000
#define windowShift 5
#define windowSize 10
short detectSpot_init_called = 0;
double smoothedDistanceValues[2][bufferSize];
int bufferHead; //empty spot after the last buffer data

enum carState currentCarState;
struct Window
{
	int start;
	int end;
}currentWindow;
void DetectSpotInit()
{
	detectSpot_init_called = 1;
	currentWindow.start = 0;
	currentWindow.end = 0;
	bufferHead = 0;
	currentCarState= searchingForParkArea;
}
/*read the wheel ticks in the corresponding values*/
void readWheelTickToBuffer(int index, int count)
{
	int i;
	int wheelTick = getWheelTick();
	for(i = index; i < (index + count); i++)
	{
		smoothedDistanceValues[1][i] = wheelTick;
	}
}
/*Takes the dumped sensor data
 * right now from file
 * later will read from memory*/
int readDistance()
{
	/*bufferHead close to 980 should trigger the cyclic queue*/
	if(bufferHead >= 980)
	{
		printf("Recycling smoothDistanceValue buffer");
		/*shift the buffer back to the beginning*/
		int index, oldBufferHead;
		oldBufferHead = bufferHead;
		bufferHead = 0;
		for(index = currentWindow.start; index <= oldBufferHead; index++)
		{
			smoothedDistanceValues[0][bufferHead++] = smoothedDistanceValues[0][index];
		}

		currentWindow.start = 0;
	}
	int newCount = ReadSonicSensorDataIntoBuffer(smoothedDistanceValues[0], bufferHead, 2);
	/*Read wheel tick for this sonic sensor read
	 * Assuming the car is moving slowly enough that the distance calculated is not off by too much*/
	readWheelTickToBuffer(bufferHead, newCount);
	if(newCount > 0)
	{
		bufferHead+= newCount;
		return 1;
	}
	else
	{
		return 0;
	}
}

/*move analyzing window by predetermined amount
 * Window of size 10 should be appropriate for detecting changes in the sensor readings
 * window shift of 5 should be okay*/
void moveWindow()
{
	currentWindow.start += windowShift;
	/*if((currentWindow.end - currentWindow.start) >= 14 )
		currentWindow.start += 5;*/
}
double findClosestToCarInWindow()
{
	double retValue = smoothedDistanceValues[0][currentWindow.start];
	int index;
	for(index = 0; index< windowSize; index++)
	{
		if(retValue > smoothedDistanceValues[0][currentWindow.start + index])
			retValue = smoothedDistanceValues[0][currentWindow.start + index];
	}

	return retValue; //in cm
}
double findFurthestToCarInWindow()
{
	double retValue = smoothedDistanceValues[0][currentWindow.start];
	int index;
	for(index = 0; index< windowSize; index++)
	{
		if(retValue < smoothedDistanceValues[0][currentWindow.start + index])
			retValue = smoothedDistanceValues[0][currentWindow.start + index];
	}

	return retValue; //in cm
}

/*generates the location of the farthest side wall of the spot based
 * 1) the spot is on the right : 1. the spot is on the left : 0
 * 2) distance to farthest side wall of the spot . +ive is forward and -ive is backward
 * 3) at what depth from the car is the farthest side wall of the spot
 *
 * */
void getSpotLocation(int side, double distanceToFarEdge, double depthToFarEdge, struct spotProperties *sp)
{

	if(side == 1)
	{
		depthToFarEdge = -depthToFarEdge;
	}
	printf("Y value %f X value %f \n", depthToFarEdge, distanceToFarEdge);
	double angle = atan2(depthToFarEdge, distanceToFarEdge) * 180 / M_PI;
	if(angle < 0)
		angle +=360;
	printf("angle to spot %f\n", angle);
	double distance = sqrt((distanceToFarEdge*distanceToFarEdge) + (depthToFarEdge*depthToFarEdge));

	sp->angle = angle;
	sp->distance = distance;
}
struct spotProperties startScan()
{
	printf("Staring scan");
	if(!detectSpot_init_called)
	{
		printf("DetectSpotInit() needs to be called before any of its functions are used\n");
		exit(1);
	}
	/*output from matlab function
	 * represents the similarity of data to 5 classes
	 * Class 1 : Large drop in distance.
	 * Class 2 : Little rise in distance.
	 * Class 3 : Little drop in distance.
	 * Class 4 : Large rise in distance.
	 * Class 5 : Random changes or steady state
	 * Keeping threshold at 60% for now. Might improve with better training data*/
	double b_y1[5];
	int parkSpotStartWheelTick, parkSpotEndWheelTick, currentWheelTick;
	double parkingAreaDepth = 0;
	double parkingAreaWallDistance;
	double parkingAreaBackWall;
	enum parkingSpotType spotType = notASpot;
	double distanceCovered = 0.0f;
	double widthOfSpot = 0.0f;
	int maxWindow = 50; //check for a max of 50 window. The give up
	int windowIndex = 0;
	while(windowIndex < maxWindow)
	{
		windowIndex++;
		if((bufferHead - currentWindow.start) > 9)
		{
			//printf("Going to switch\n");
			switch(currentCarState)
			{
			case 0: //searching for parking area
				/*call routine to analyze window for sharp drop in distance
				 * this method reads 10 values at a time*/
				parkNeuralNetworkFunction(&(smoothedDistanceValues[0][currentWindow.start]), b_y1);
				//printf("Case 0: ALL class matces 1 %f 2 %f 3 %f 4 %f 5 %f\n",b_y1[0],b_y1[1],b_y1[2],b_y1[3],b_y1[4]);
				/*analyze b_y1[0] to see if more than 70% match */
				if(b_y1[0] >= 0.7f)
				{
					//printf("Start of Parking area found\n");

					/*change car state*/
					currentCarState = searchingForStartOfSpace;
					break;
				}
				//break; //allow fall through
			case 1: //searching for parking spot
				/*call routine to analyze window for empty parking space*/
				if(currentCarState == searchingForStartOfSpace)
				{
					parkNeuralNetworkFunction(&(smoothedDistanceValues[0][currentWindow.start]), b_y1);
					//printf("Case 1: ALL class matces 1 %f 2 %f 3 %f 4 %f 5 %f\n",b_y1[0],b_y1[1],b_y1[2],b_y1[3],b_y1[4]);
				}
				/*analyze b_y1[1] to see if more than 70% match*/
				if(b_y1[1] >= 0.7f)
				{
					//printf("Start of a parking spot found\n");
					//printf("Buffer index of parking spot %i\n", (currentWindow.start));
					int temp;
					/*printf("In the window\n");
					for(temp = 0; temp < windowSize; temp++)
					{
						printf("%f ", smoothedDistanceValues[0][temp]);
					}
					printf("\n");*/
					parkSpotStartWheelTick = smoothedDistanceValues[1][currentWindow.start];
					printf("parkSpotStartWheelTick %i ", smoothedDistanceValues[1][currentWindow.start]);
					/*Find perpendicular distance to the closes wall object
					 * This will be used to determine depth of spot
					 */
					parkingAreaWallDistance = findClosestToCarInWindow();
					//printf("Parking area wall distance %f cm", parkingAreaWallDistance);
					/*Find the perpendicular distance to the area back wall
					 * This will be used to determine depth of spot*/
					parkingAreaBackWall = findFurthestToCarInWindow();
					//printf("Parking area backwall distance %f cm", parkingAreaBackWall);
					parkingAreaDepth = parkingAreaBackWall - parkingAreaWallDistance;


					/*If parkingAreaDepth is less than the car length then set parking mode to parallel
					 * And run sensor till it crosses car length*/
					//if(parkingAreaDepth <= (vehicle_length + vehicle_buffer_around))
					if(parkingAreaDepth <= 6.0f)
					{
						//printf("Spot is for parallel parking\n");
						printf("Parking area wall distance %f cm.\nParking area backwall distance %f cm.\nParking Area depth seems to be around : %f cm. Parking area parallel\n",parkingAreaWallDistance,parkingAreaBackWall, parkingAreaDepth);
						spotType = parallel;
					}
					else
					{
						printf("Parking area wall distance %f cm.\nParking area backwall distance %f cm.\nParking Area depth seems to be around : %f cm. Parking area perpendicular\n",parkingAreaWallDistance,parkingAreaBackWall, parkingAreaDepth);

						spotType = angle;
					}
					/*Take the count of current wheel tick*/
					currentCarState = detectingSpaceDimensions;


					/*follow conservative calculation and assume the end index of the window to be the start*/
					//parkSpotStartIndex =  currentWindow.start + 10; //WARNING: this will fail when buffer is recycled or when buffer is almost full
				}
				break;
			case 2: //found
				/*call routine to detect end of space
				 * at this point the vehicle might detect a small drop in distance(the end of the free space)
				 * OR directly a sharp rise(end of the parking area)
				 * This depends on the speed of the vehicle*/
				parkNeuralNetworkFunction(&(smoothedDistanceValues[0][currentWindow.start]), b_y1);

				//printf("Case 2: ALL class matces 1 %f 2 %f 3 %f 4 %f 5 %f\n",b_y1[0],b_y1[1],b_y1[2],b_y1[3],b_y1[4]);
				if(b_y1[2] >= 0.7f || b_y1[3] >= 0.7f) //probably won't make a difference between end of spot and area
				{
					parkSpotEndWheelTick = smoothedDistanceValues[1][currentWindow.start];
					currentWheelTick = getWheelTick();
					printf("Latest wheel tick %i\n",currentWheelTick );
					distanceCovered = wheelTicksToDistance(currentWheelTick - parkSpotStartWheelTick);
					widthOfSpot = wheelTicksToDistance(parkSpotEndWheelTick - parkSpotStartWheelTick);

					//STOP CAR MOVEMENT HERE
					carStop();
					//printf("End of Area Found. ");
					struct spotProperties sp;
					sp.width = widthOfSpot;
					if(b_y1[2] >= 0.7f)
					{
						//printf("End was end of spot\n");
						printf("Width of spot is %f \n",widthOfSpot);
						if(spotType == angle)//check if car width can fit
						{
							if(distanceCovered > (vehicle_width + vehicle_buffer_around))
							{
								printf("Spot found\n");
								currentCarState =  foundSpot; //not really necessary. Just for clarity

								getSpotLocation(1, -distanceCovered, parkingAreaWallDistance, &sp);
								sp.spotType = angle;
								return sp;
							}
							else
							{
								printf("Spot not wide enough for parking. Move Car and search\n");
								currentCarState = searchingForStartOfSpace;
								//START CAR MOVEMENT HERE
								carMoveForward();
								continue;
							}
						}
						else //spot type should be parallel
						{
							if(distanceCovered > vehicle_length)
							{
								printf("Spot found\n");
								currentCarState = foundSpot;
								getSpotLocation(1, -distanceCovered, parkingAreaWallDistance, &sp);
								sp.spotType = parallel;
								return sp;
							}
							else
							{
								printf("Spot not long enough for parking.. Move Car and search\n");
								currentCarState = searchingForStartOfSpace;
								//START CAR MOVEMENT HERE
								carMoveForward();
								continue;
							}
						}
					}
					else
					{
						//printf("End was end of parking Area\n");
						if(spotType == angle)//check if car width can fit
						{
							if(distanceCovered > (vehicle_width + vehicle_buffer_around))
							{
								printf("Spot found");
								currentCarState =  foundSpot; //not really necessary. Just for clarity
								getSpotLocation(1, -distanceCovered, parkingAreaWallDistance, &sp);
								sp.spotType = angle;
								return sp;
							}
							else
							{
								printf("No spot found. Stopping search");
								currentCarState = searchingForParkArea; //will not matter
								sp.spotType = notASpot;
								return sp;
							}
						}
						else //spot type should be parallel
						{
							if(distanceCovered > (vehicle_length + (vehicle_buffer_around*2)))
							{
								printf("Spot found");
								currentCarState = foundSpot;
								getSpotLocation(1, -distanceCovered, parkingAreaWallDistance, &sp);
								sp.spotType = parallel;
								return sp;
							}
							else
							{
								printf("No spot found. Stopping search");
								currentCarState = searchingForParkArea; //will not matter
								sp.spotType = notASpot;
								return sp;
							}
						}
					}
				}
				else //if(b_y1[4] > 0.7f) //if no pattern found, then calculate the length of empty spot
				{
					int currentWheelTick = smoothedDistanceValues[1][currentWindow.start];
					currentWheelTick = getWheelTick();

					distanceCovered = wheelTicksToDistance(currentWheelTick - parkSpotStartWheelTick);
					printf("Last wheel tick %i. Ditance covered %f\n",currentWheelTick, distanceCovered );
					widthOfSpot = wheelTicksToDistance(currentWheelTick - parkSpotStartWheelTick);

					struct spotProperties sp;
					sp.width = widthOfSpot;
					if(spotType == angle)
					{
						if(distanceCovered > (vehicle_width + vehicle_buffer_around))
						{
							printf("Spot found");
							getSpotLocation(1, -distanceCovered, parkingAreaWallDistance, &sp);
							sp.spotType = angle;
							return sp;
						}
						else
						{
							printf("No spot found yet. Move Car and search\n");
							continue;
						}
					}
					else
					{
						if(distanceCovered > (vehicle_length + (vehicle_buffer_around*2)))
						{
							printf("Spot found");
							getSpotLocation(1, -distanceCovered, parkingAreaWallDistance, &sp);
							sp.spotType = parallel;
							return sp;
						}
						else
						{
							printf("No spot found yet. Move Car and search\n");

							continue;
						}
					}
				}
				break;
			}

			moveWindow();
		}
		else /*when there are less than 10 data in the buffer*/
		{
			printf("DetectSpot: Call to readDistance\n");
			readDistance();
		}
	}
}
