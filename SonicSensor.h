/*
 * SonicSensor.h
 *
 *  Created on: Jan 22, 2016
 *      Author: Anjum PArvez Ali
 */

#ifndef SONICSENSOR_H_
#define SONICSENSOR_H_

int ReadSonicSensorDataIntoBuffer(double* smoothBuffer, int index, int sensorId);
/*Fires all sensors
 * return value in array
 * parameter: pointer to array in which the sensor readings will be returned*/
void readAllRangeSensor(int* buffer);

#endif /* SONICSENSOR_H_ */
