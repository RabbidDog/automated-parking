/*
 * ParkingModule.c
 *
 *  Created on: Mar 5, 2016
 *      Author: Anjum Parvez Ali / Joshua Koutny
 */


#include "ParkingModule.h"


void ParkingModuleInit()
{

}


void createCommandForAngleParking(struct spotProperties properties)
{
	/*pre determined steps for parking
	 * First move back and allign car centre with the end of parking spot
	 * Second move forward the vehicle away from the spot, almost making it perpendicular to the spot
	 * Then slowly back and adjust to avoid obstacle*/

	printf("Spot at an angle of %f at a distance %f \n",properties.angle, properties.distance);
	/*Step 1.0
	 * 1.1 Drive back to a position where the middle of the car is alligned with the end of spot*/
	struct movementCommand step101, step102, step103;
	if(properties.angle > 180 && properties.angle < 270)
	{
		printf("spot between 180 and 270 at a distance of %f \n", properties.distance);

		double straight_drive_back  = cos(properties.angle - 180)* properties.distance;
		if(straight_drive_back < 0)
			straight_drive_back = -straight_drive_back;
		/*We only want to back up to a position where the end of the car is alligned with the end of spot*
		 */
		straight_drive_back -= (vehicle_length /2);
		if(straight_drive_back < 0)
			straight_drive_back =0;
		printf("Go back %f cm\n", straight_drive_back);
		carMoveBackwards(straight_drive_back);
		carMoveForwardTo(0, 90);
		refreshMap();

		while(mapDistanceWeightedAngularCell[4] > 1)
		{
			carMoveBackwards(2.0f);
		}
		carStop();
		/*carMoveBackwardTo(step101.straight_distance, step101.final_angle_change);
		printf("Calculated steps 101 turn angle %f and move %f cm\n",step101.final_angle_change, step101.straight_distance);
		printf("Calculated steps 102 turn angle %f and move %f cm\n",step102.final_angle_change, step102.straight_distance);
		printf("Calculated steps 103 turn angle %f and move %f cm\n",step103.final_angle_change, step103.straight_distance);

		carMoveBackwardTo(step102.straight_distance, step102.final_angle_change);
		carMoveBackwardTo(step103.straight_distance, step103.final_angle_change);*/
	}


	/*refresh map and spot location to check if step 1.0 was completed precisely*/
	//refreshMap();
	/*update spot location*/
	/*find direction and its region with the farthest obstacle*/
	//int regionIndexToCheck[] = {0,1,7};
	//int index;
	/*for(index = 0; index < 3; index++)
	{

	}*/

}

void createCommandForParallelParking(struct spotProperties properties)
{

}


void startToPark(struct spotProperties properties)
{
	/*Right at this moment of the development process the vehicle is able to process and detect paring space very quickly
	 * This means the car is not too far ahead of the parking spot when the sonic sensor data are analyzed
	 * So we will proceed with the notion that when this method is called, the parking spot is very close
	 */
	if(properties.spotType == angle)
	{
		createCommandForAngleParking(properties);
	}
}
