/*
 * HMC5883L.h
 *	I2Cdev library collection - HMC5883L I2C device class
 *  Created on: Nov 8, 2015
 *      Author: Anjum Parvez Ali
 */



#define HMC5883L_Mode_Register		0x02
#define HMC5883L_X_MSB_Register		0x03
#define HMC5883L_X_LSB_Register		0x04
#define HMC5883L_Y_MSB_Register		0x07
#define HMC5883L_Y_LSB_Register		0x08
#define HMC5883L_Z_MSB_Register		0x05
#define HMC5883L_Z_LSB_Register		0x06
#define HMC5883L_Status_Register	0x09
#define HMC5883L_Configuration_Register_A	0x0
#define HMC5883L_Configuration_Register_B	0x1

#define HMC5883L_Status_Register_RDY		0

#define Magnetic_Declination 		.05003


struct Magnetometer
{
	float X_Axis;
	float Z_Axis;
	float Y_Axis;
};

enum Magnetometer_Scale
{
	//actual scale value is 1/100 of the enum values
	scale088 = 88,
	scale13 = 130,
	scale19 = 190,
	scale25 = 250,
	scale40 = 400,
	scale47 = 470,
	scale56 = 560,
	scale81 = 810
};

enum Magnetometer_Mode
{
	continuous = 0,
	single = 1
};

void initMagnetometer();
void SetMeasurementMode(enum Magnetometer_Mode mode);
void SetScale(enum Magnetometer_Scale scale);
void EnableBypassI2C();
/*
 * 1 for ready
 * 0 for not ready*/
unsigned int IsMagnetometerDataReady();
struct Magnetometer ReadRawAxis();
struct Magnetometer ReadScaledAxis();
float GetMagnetometerHeading();
