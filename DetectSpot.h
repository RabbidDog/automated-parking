/*
 * DetectSpot.h
 *
 *  Created on: Jan 22, 2016
 *      Author: RabbidDog
 */

#ifndef DETECTSPOT_H_
#define DETECTSPOT_H_
#include "head.h"
#include <math.h>
enum carState
{
	searchingForParkArea,
	searchingForStartOfSpace,
	detectingSpaceDimensions,
	foundSpot
};

void DetectSpotInit();

struct spotProperties startScan();
#endif



