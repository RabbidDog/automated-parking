/*
 * GenerateMap.c
 *
 *  Created on: 22.01.2016
 *      Author: Anjum Parvez Ali / Joshua Koutny
 */
#include <stdio.h>
#include <stdlib.h>
#include "GenerateMap.h"
#include "VehicleProperty.h"
#include "SonicSensor.h"

const int region = 7;
const int direction = 8;
short generateMap_init_called=0;
float * wheelBuffer;
float depth, width;
const int MINPARALLELDEPTH = 25;
const int MINORTHOGONALDEPTH = 30;
const int MINPARALLELWIDTH = 40;
const int MINORTHOGONALWIDTH = 25;
char* mapDistanceWeightedAngularCell;
const double region_delimeter[7] = {2.0f, 4.0f, 6.0f, 8.0f, 16.0f, 32.0f, 64.0f};
const double direction_delimeter[8] = {10.0f, 30.0f, 150.0f, 170.0f, 190.0f, 210.0f, 330.0f, 350.0f}; //the roof angle of the direction. Direction 1 is from 350 to 10 deg

/*Alternative approach is to have a distance weighted angular cell map
 * The whole environment is divided into angle region of 15deg, but since we are not building
 * a complete map using history data, we use only 8 directions(corresponding to 8 sensors)
 * That provides us 8 directions.
 * Each direction has cells in it which cover more distance as we move away from the car, increasing in the order of 2
 * So the first cell will cover  2cm, 2nd cell 4cm, 3rd cell 8cm
 * We allow 7 cells per region to cover 128cm radius around our car(starting from the sensor body and not from car (0,0)).
 * The region goes from 0 to 360 in clock wise direction*/
void GenerateMapInit()
{
	generateMap_init_called = 1;
	int index;
	int directionIndex, regionIndex;
	mapDistanceWeightedAngularCell = (char*)malloc(direction * sizeof(char));
	for(directionIndex = 0; directionIndex < direction; directionIndex++)
	{
		mapDistanceWeightedAngularCell[directionIndex] &= 0;
	}
	/*mapDistanceWeightedAngularCell =(short**) malloc(direction * sizeof(short *));
	for(index = 0; index < direction; index++)
	{
		mapDistanceWeightedAngularCell[index] = (short*)malloc(region * sizeof(short));
	}*/

	/*for(directionIndex = 0; directionIndex < direction ; directionIndex++)
	{
		memset(&mapDistanceWeightedAngularCell[directionIndex], 0, region*sizeof(short));
	}*/
}
void setParkingSpotCoordinate(struct spotProperties *sp)
{
	//spot_property = *sp;
}
void refreshMap()
{
	if(!generateMap_init_called)
	{
		printf("GenerateMapInit() needs to be called before any of its functions are used\n");
		exit(1);
	}
	printf("Refreshing Environment Map\n");
	int index;
	int sensorReadingBuffer[8];
	readAllRangeSensor(sensorReadingBuffer);
	/*clear map*/
	int directionIndex;
	for(directionIndex = 0; directionIndex < direction ; directionIndex++)
	{
		mapDistanceWeightedAngularCell[direction] &= 0;
		printf("Cleared direction %i. value now %i\n", directionIndex, mapDistanceWeightedAngularCell[directionIndex]);
	}
	/*for(directionIndex = 0; directionIndex < direction ; directionIndex++)
	{
		memset(&mapDistanceWeightedAngularCell[directionIndex], 0, region*sizeof(short));
	}*/

	for(index = vehicle_sensor_count-1; index >-1  ; index--)
	{
		int directionIndex = -1;
		int regionIndex = -1;
		double *transformedReading =  getRadialTransformedPosition(index, sensorReadingBuffer[index]);
		/*below angles can be hard-coded independent of the actual angle the sensors are facing
		 * because we can decide what regions we want the environment to be divided into*/
		printf("Sensor 5i detected values . angle %f  distance %f \n", index, transformedReading[1], transformedReading[0]);
		for(regionIndex = region-1; regionIndex > -1; regionIndex--)
		{
			if(transformedReading[0] >= region_delimeter[regionIndex])
				break;
		}
		for(directionIndex = direction-1; directionIndex > -1; directionIndex--)
		{
			if(transformedReading[1] >= direction_delimeter[directionIndex])
			{
				directionIndex = (directionIndex + 1) % direction;
				break;
			}

		}
		printf("For Sensor %i obstacle found between %f and %f and at a distance above %f\n", index, direction_delimeter[directionIndex-1], direction_delimeter[directionIndex], region_delimeter[regionIndex]);
		mapDistanceWeightedAngularCell[directionIndex] |= 1<<region;
		printf("Set map value to %i\n", mapDistanceWeightedAngularCell[directionIndex]);
	}
}

void deallocMap()
{
	if(!generateMap_init_called)
	{
		printf("GenerateMapInit() needs to be called before any of its functions are used\n");
		exit(1);
	}
	free(mapDistanceWeightedAngularCell);
}

/*
 * calculate the width and depth of the parking spot and decide
 * whether orthogonal, parallel parking or both are possible
 */
enum parkingSpotType calcDimensions(double * scanBuffer, int startIndex, int endIndex)
{
	if(!generateMap_init_called)
	{
		printf("GenerateMapInit() needs to be called before any of its functions are used\n");
		exit(1);
	}
	enum parkingSpotType spotType = notASpot;
	depth = scanBuffer[startIndex] - scanBuffer[startIndex - 10];

	if (depth < MINPARALLELDEPTH)
		return spotType;

	float wheelticks = wheelBuffer[endIndex] - wheelBuffer[startIndex];

	width = wheelticks * 65.0f * 3.14f / 18.0f;

	if (width > MINPARALLELWIDTH && depth > MINORTHOGONALDEPTH)
		spotType = both ; // both orthogonal and parallel parking possible here
	else if (width > MINPARALLELWIDTH && depth > MINPARALLELDEPTH)
		spotType = parallel;
	// only parallel parking possible here
	else if (width > MINORTHOGONALWIDTH && depth > MINORTHOGONALDEPTH)
		spotType = angle;
	// only orthogonal parking possible here

	return spotType;
}
