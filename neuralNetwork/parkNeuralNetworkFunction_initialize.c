/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: parkNeuralNetworkFunction_initialize.c
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 11-Jan-2016 01:22:18
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "parkNeuralNetworkFunction.h"
#include "parkNeuralNetworkFunction_initialize.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void parkNeuralNetworkFunction_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * File trailer for parkNeuralNetworkFunction_initialize.c
 *
 * [EOF]
 */
